/*
 * Kernel module for testing utf-8 support.
 *
 * Copyright 2017 Collabora Ltd. All Rights Reserved
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/module.h>
#include <linux/printk.h>
#include <linux/utf8norm.h>

unsigned int failed_tests;
unsigned int total_tests;

#define _test(cond, func, line, fmt, ...) do {				\
		total_tests++;						\
		if (!cond) {						\
			failed_tests++;					\
			pr_err("test %s:%d Failed: %s%s",		\
			       func, line, #cond, (fmt?":":"."));	\
			if (fmt)					\
				pr_err(fmt, ##__VA_ARGS__);		\
		}							\
	} while (0)
#define test_f(cond, fmt, ...)  _test(cond, __func__, __LINE__, fmt, ##__VA_ARGS__)
#define test(cond) _test(cond, __func__, __LINE__, "")

const static struct {
	/* UTF-8 strings in this vector _must_ be NULL-terminated. */
	unsigned char str[10];
	unsigned char dec[10];
} nfkdi_test_data[] = {
	/* Trivial sequence */
	{
		.str = {0x41, 0x42, 0x62, 0x61, 0x00},		/* "ABba" decomposes to itself */
		.dec = {0x41, 0x42, 0x62, 0x61, 0x00}
	},
	/* Simple equivalent sequences */
	{
		.str = {0xc2, 0xbc, 0x00},			/* 'VULGAR FRACTION ONE QUARTER' decomposes to */
		.dec = {0x31, 0xe2, 0x81, 0x84, 0x34, 0x00},	/* 'NUMBER 1' + 'FRACTION SLASH' + 'NUMBER 4' */
	},
	{
		.str = {0xc3, 0xa4, 0x00},			/* 'LATIN SMALL LETTER A WITH DIAERESIS' decomposes to */
		.dec = {0x61, 0xcc, 0x88, 0x00},		/* 'LETTER A' + 'COMBINING DIAERESIS' */
	},
	{
		.str = {0xC7, 0x89, 0x00},			/* 'LATIN SMALL LETTER LJ' decomposes to */
		.dec = {0x6c, 0x6a, 0x00},			/* 'LETTER L' + 'LETTER J' */
	},
	/* Canonical ordering */
	{
		.str = {0x41, 0xcc, 0x81, 0xcc, 0xa8, 0x0},	/* A + 'COMBINING ACUTE ACCENT' + 'COMBINING OGONEK' decomposes to */
		.dec = {0x41, 0xcc, 0xa8, 0xcc, 0x81, 0x0},	/* A + 'COMBINING OGONEK' + 'COMBINING ACUTE ACCENT' */
	}
};

const static struct {
	/* UTF-8 strings in this vector _must_ be NULL-terminated. */
	unsigned char str[30];
	unsigned char ncf[30];
} nfkdicf_test_data[] = {
	/* Trivial sequences */
	{
		.str = {0x41, 0x42, 0x62, 0x61, 0x00},	/* "ABba" folds to lowercase */
		.ncf = {0x61, 0x62, 0x62, 0x61, 0x00},
	},
	{
		.str = "ABCDEFGHIJKLMNOPRSTUVWXYZ0.1",	/* All ASCII folds to lower-case */
		.ncf = "abcdefghijklmnoprstuvwxyz0.1",
	},
	{
		.str = {0xc3, 0x9f, 0x00}, 		/* LATIN SMALL LETTER SHARP S folds to */
		.ncf = {0x73, 0x73, 0x00},		/* LATIN SMALL LETTER S + LATIN SMALL LETTER S */
	},
	{
		.str = {0xC3, 0x85, 0x0},		/* LATIN CAPITAL LETTER A WITH RING ABOVE folds to */
		.ncf = {0x61, 0xcc, 0x8a, 0x0},		/* LATIN SMALL LETTER A + COMBINING RING ABOVE */
	}
};

static void check_utf8_nfkdi(void)
{
	int i;
	struct utf8cursor u8c;
	const struct utf8data *data = utf8nfkdi(UNICODE_AGE(7, 0, 0));

	for (i = 0; i < ARRAY_SIZE(nfkdi_test_data); i++) {
		int len = strlen(nfkdi_test_data[i].str);
		int nlen = strlen(nfkdi_test_data[i].dec);
		int j = 0;
		unsigned char c;

		test((utf8len(data, nfkdi_test_data[i].str) == nlen));
		test((utf8nlen(data, nfkdi_test_data[i].str, len) == nlen));

		if (utf8cursor(&u8c, data, nfkdi_test_data[i].str) < 0)
			pr_err("can't create cursor\n");

		while ((c = utf8byte(&u8c)) > 0) {
			test_f((c == nfkdi_test_data[i].dec[j]),
			       "%x %x\n", c, nfkdi_test_data[i].dec[j]);
			j++;
		}

		test((j == nlen));
	}
}

static void check_utf8_nfkdicf(void)
{
	int i;
	struct utf8cursor u8c;
	const struct utf8data *data = utf8nfkdicf(UNICODE_AGE(7, 0, 0));

	for (i = 0; i < ARRAY_SIZE(nfkdicf_test_data); i++) {
		int len = strlen(nfkdicf_test_data[i].str);
		int nlen = strlen(nfkdicf_test_data[i].ncf);
		int j = 0;
		unsigned char c;

		test((utf8len(data, nfkdicf_test_data[i].str) == nlen));
		test((utf8nlen(data, nfkdicf_test_data[i].str, len) == nlen));

		if (utf8cursor(&u8c, data, nfkdicf_test_data[i].str) < 0)
			pr_err("can't create cursor\n");

		while ((c = utf8byte(&u8c)) > 0) {
			test_f((c == nfkdicf_test_data[i].ncf[j]),
			       "0x%x 0x%x\n", c, nfkdicf_test_data[i].ncf[j]);
			j++;
		}

		test((j == nlen));
	}
}

static void check_supported_versions(void)
{
	/* Unicode 7.0.0 should be supported. */
	test(utf8version_is_supported(UNICODE_AGE(7, 0, 0)));

	/* Unicode 8.0.0 should not be supported. */
	test(!utf8version_is_supported(UNICODE_AGE(8, 0, 0)));

	/* Next values don't exist. */
	test(!utf8version_is_supported(UNICODE_AGE(0, 0, 0)));
	test(!utf8version_is_supported(UNICODE_AGE(-1, -1, -1)));
}

static int __init init_test_ucd(void)
{
	failed_tests = 0;
	total_tests = 0;

	check_supported_versions();
	check_utf8_nfkdi();
	check_utf8_nfkdicf();

	if (!failed_tests)
		pr_info("All %u tests passed\n", total_tests);
	else
		pr_err("%u out of %u tests failed\n", failed_tests,
		       total_tests);
	return 0;
}

static void __exit exit_test_ucd(void)
{
}

module_init(init_test_ucd);
module_exit(exit_test_ucd);

MODULE_AUTHOR("Gabriel Krisman Bertazi <krisman@collabora.co.uk>");
MODULE_LICENSE("GPL");
