#ifndef _LINUX_FSUCD_H
#define _LINUX_FSUCD_H

#include <linux/utf8norm.h>

inline unsigned fsucd_get_magic_number(int major, int minor, int rev,
				       unsigned int *version)
{
	unsigned age = UNICODE_AGE(major, minor, rev);

	if (!utf8version_is_supported(age))
		return -1;

	*version = age;
	return 0;
}

inline int fsucd_strcmp(int utf8_version, char *str1, char *str2)
{
	const struct utf8data *data = utf8nfkdi(utf8_version);
	struct utf8cursor cur1, cur2;
	unsigned char c1, c2;
	int r;

	r = utf8cursor(&cur1, data, str1);
	if (r < 0)
		return -EIO;

	r = utf8cursor(&cur2, data, str2);
	if (r < 0)
		return -EIO;

	do {
		c1 = utf8byte(&cur1);
		c2 = utf8byte(&cur2);

		if (c1 != c2)
			return 1;

	} while (c1 && c2);

	return 0;
}

inline int fsucd_norm_strncmp(int utf8_version, char *c_str,
			      char *normcmp, int norm_len)
{
	const struct utf8data *data = utf8nfkdi(utf8_version);
	struct utf8cursor cur;
	unsigned char c;
	int r, i;

	r = utf8cursor(&cur, data, c_str);
	if (r < 0)
		return -EIO;

	for (i = 0; i < norm_len; i++) {

		c = utf8byte(&cur);

		if (!c || c != normcmp[i])
			return 1;
	}

	return 0;
}

inline int fsucd_strcasecmp(int utf8_version, char *str1, char *str2)
{
	const struct utf8data *data = utf8nfkdicf(utf8_version);
	struct utf8cursor cur1, cur2;
	unsigned char c1, c2;
	int r;

	r = utf8cursor(&cur1, data, str1);
	if (r < 0)
		return -EIO;

	r = utf8cursor(&cur2, data, str2);
	if (r < 0)
		return -EIO;

	do {
		c1 = utf8byte(&cur1);
		c2 = utf8byte(&cur2);

		if (c1 != c2)
			return 1;

	} while (c1 && c2);

	return 0;
}

inline int fsucd_norm_strncasecmp(int utf8_version, char *c_str,
				  char *normcmp, int norm_len)
{
	const struct utf8data *data = utf8nfkdicf(utf8_version);
	struct utf8cursor cur;
	unsigned char c;
	int r, i;

	r = utf8cursor(&cur, data, c_str);
	if (r < 0)
		return -EIO;

	for (i = 0; i < norm_len; i++) {

		c = utf8byte(&cur);

		if (!c || c != normcmp[i])
			return 1;
	}

	return 0;
}

#endif

